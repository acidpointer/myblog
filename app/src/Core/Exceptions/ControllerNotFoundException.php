<?php
namespace HNF\Core\Exceptions;

use HNF\Core\Interfaces\HydroException;

class ControllerNotFoundException implements HydroException
{
}