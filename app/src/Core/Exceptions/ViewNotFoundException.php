<?php
namespace HNF\Core\Exceptions;

use HNF\Core\Interfaces\HydroException;

class ViewNotFoundException implements HydroException
{
}