<?php
namespace HNF\Core\Exceptions;

use HNF\Core\Interfaces\HydroException;

class ModelNotFoundException implements HydroException
{
}