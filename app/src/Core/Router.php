<?php
namespace HNF\Core;

use \Nyholm\Psr7\Factory\Psr17Factory;
use \Nyholm\Psr7\ServerRequest;
use \Spiral\RoadRunner\PSR7Client;

use HNF\Core\Exceptions\ControllerNotFoundException;

class Router
{
    public const CONTROLLER_REGEX = "/^[a-zA-Z0-9]+$/";
    public const METHOD_REGEX     = "/^[a-zA-Z0-9]+$/";
    public const ARGUMENT_REGEX   = "/^[a-zA-Z0-9=&?]+$/";

    public const RESERVED_PAGES = ['main', '400', '401', '403', '404', '500', '503'];

    private array $pages;

    private PSR7Client $psr7;
    private Psr17Factory $psr17Factory;

    public function __construct($pages)
    {
        $this->pages = $pages;
    }

    public function route(string $request)
    {
        if ($this->pages['main'] === null) {
            throw new ControllerNotFoundException('Any site should have main page! Please, register page with alias "main"!');
        }

        $req = explode('/', $request);
        $reqClass = preg_match(self::CONTROLLER_REGEX, $req[1]) ? $req[1] : '';
        $reqMethod = preg_match(self::METHOD_REGEX, $req[2]) ? $req[2] : '';
        $reqArgument = preg_match(self::ARGUMENT_REGEX, $req[3]) ? $req[3] : '';
     
        $modelClass = '\HNF\Models\\'.$reqClass;
        $viewClass = '\HNF\Views\\'.$reqClass;
        $controllerClass = '\HNF\Controllers\\'.$reqClass;

        $model = new $modelClass;
        $view = new $viewClass;
        $controller = new $controllerClass($model, $view);

        return $controller->$reqMethod($reqArgument);
    }
}