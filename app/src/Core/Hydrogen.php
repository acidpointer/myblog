<?php
namespace HNF\Core;

use Nyholm\Psr7\Factory\Psr17Factory;
use Spiral\RoadRunner\PSR7Client;

use HNF\Core\Router;

class Hydrogen
{
    private PSR7Client $psr7;
    private Psr17Factory $psr17Factory;

    private array $pages;

    public function __construct(array $pages)
    {
        $this->pages = $pages;

        $relay = new \Spiral\Goridge\StreamRelay(STDIN, STDOUT);
        $worker = new \Spiral\RoadRunner\Worker($relay);

        $this->psr17Factory = new \Nyholm\Psr7\Factory\Psr17Factory();
        $this->psr7 = new \Spiral\RoadRunner\PSR7Client($worker, $this->psr17Factory, $this->psr17Factory, $this->psr17Factory);
    }

    public function run()
    {
        $router = new Router($this->pages);

        while ($req = $this->psr7->acceptRequest()) {
            try {
                $msg = 'Hydrogen loaded!';

                $msg = (string) $router->route($req->getServerParams()['REQUEST_URI']);
                $resp = $this->psr17Factory->createResponse();
                $resp->getBody()->write($msg);
                $this->psr7->respond($resp);
            } catch (\Throwable $e) {
                // TODO: Show 500 - Internal server error


                $this->psr7->getWorker()->error('ERR!');
            }
        }
    }
}