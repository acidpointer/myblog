<?php
namespace HNF\Core\Interfaces;

interface View
{
    public function render();
}