<?php
namespace HNF\Controllers;

use \HNF\Core\Interfaces\View;
use \HNF\Core\Interfaces\Model;

class Error404
{
    private Model $model;
    private View $view;

    public function __construct(Model $model, View $view)
    {
        $this->model = $model;
        $this->view = $view;
    }
}