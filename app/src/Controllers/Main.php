<?php
namespace HNF\Controllers;

use \HNF\Core\Interfaces\Model;
use \HNF\Core\Interfaces\View;
use \HNF\Core\Interfaces\Controller;

class Main implements Controller
{
    private Model $model;
    private View $view;

    public function __construct($model, $view)
    {
        $this->model = $model;
        $this->view = $view;
    }
}