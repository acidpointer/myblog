<?php
namespace HNF\Views;

use \Nyholm\Psr7\Factory\Psr17Factory;
use \Spiral\RoadRunner\PSR7Client;

use \HNF\Core\Interfaces\View;

class Main implements View
{
    private Psr17Factory $psr17Factory;
    private PSR7Client $psr7;

    public function __construct(Psr17Factory $psr17Factory, PSR7Client $psr7)
    {
        $this->psr17Factory = $psr17Factory;
        $this->psr7 = $psr7;
    }


    
}