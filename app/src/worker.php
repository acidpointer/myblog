<?php 
//  _   _           _                            
// | | | |         | |                           
// | |_| |_   _  __| |_ __ ___   __ _  ___ _ __  
// |  _  | | | |/ _` | '__/ _ \ / _` |/ _ \ '_ \ 
// | | | | |_| | (_| | | | (_) | (_| |  __/ | | |
// \_| |_/\__, |\__,_|_|  \___/ \__, |\___|_| |_|
//         __/ |                 __/ |           
//        |___/                 |___/   by acidpointer         
//

declare(strict_types=1);

ini_set('display_errors', 'stderr');

require '/var/www/vendor/autoload.php';

# Composer has good classloader, we use it!
$loader = new \Composer\Autoload\ClassLoader();
$loader->addPsr4('HNF\Core\\', __DIR__.'/Core');
$loader->addPsr4('HNF\Core\\Exceptions\\', __DIR__.'/Core/Exception');
$loader->addPsr4('HNF\Core\\Interfaces\\', __DIR__.'/Core/Interfaces');
$loader->addPsr4('HNF\Models\\', __DIR__.'/Models');
$loader->addPsr4('HNF\Views\\', __DIR__.'/Views');
$loader->addPsr4('HNF\Controllers\\', __DIR__.'/Controllers');

$loader->register();
$loader->setUseIncludePath(true);

// **************************** //
use \HNF\Core\Hydrogen;

define('DEBUG', true);

$pages = array(
    'main' => 'Main',
    '404' => 'Error404'
);

$h = new Hydrogen($pages);
$h->run();